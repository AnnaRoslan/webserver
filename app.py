from flask_hal import HAL, document
from flask_hal.link import Link
from flask_hal.document import Document, Embedded
from flask_cors import CORS
from flask import Flask, request, render_template,send_from_directory, make_response, session, flash, url_for, g
from dotenv import load_dotenv
from os import getenv
from bcrypt import hashpw,gensalt, checkpw
from flask_session import Session
from datetime import datetime, timedelta
import secrets
from redis import StrictRedis
from jwt import decode
import json
import uuid
import requests
import pika

load_dotenv()
REDIS_HOST = getenv("REDIS_HOST")
REDIS_PASS = getenv("REDIS_PASS")
JWT_SECRET = getenv("JWT_SECRET")
JWT_EXP = int(getenv("JWT_EXP"))

db = StrictRedis(REDIS_HOST, db=20, password=REDIS_PASS)

SESSION_TYPE = "redis"
SESSION_REDIS = db
app = Flask(__name__, static_url_path="/static")
app.config.from_object(__name__)
app.secret_key = secrets.token_urlsafe(32)
HAL(app)
sess = Session(app)

NOTIFI_SERVER_URL = 'https://sheltered-reaches-07912.herokuapp.com/'

######################################## Queue #######################
Q_HOST =  getenv("Q_HOST")
Q_CRED = getenv("Q_CRED")
Q_PORT = int(getenv("Q_PORT"))
Q_VH = getenv("Q_VH")
credentials = pika.PlainCredentials(Q_CRED, Q_CRED)
parameters = pika.ConnectionParameters(Q_HOST, Q_PORT, Q_VH, credentials)

def send_mess_to_queue(body, routing_key):
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='logs',
                            exchange_type='topic')
    channel.basic_publish(exchange='logs',
                        routing_key=routing_key,
                        body=body)
    connection.close()
######################################################################

@app.route('/')
def main():
    return make_response("",200)
    
@app.route('/sender/labels', methods=["GET"])
def user_labels():
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'sender':
        return'Not authorized', 401
    
    user = g.user    
    labels = get_labels(user)
    links = []
    links.append(Link('Delete', 'sender/labels/{label}', templated=False ))
    links.append(Link('Create', 'sender/labels/', templated=False))
    items = []
    for key in labels:
        data = {'label':labels[key]['label'], 'status': labels[key]['state']}
        items.append(Embedded(data=data))
    
    document = Document(embedded={'items' : Embedded(data=items)},links=links)
    return document.to_json()

@app.route('/sender/labels', methods=["POST"])
def labels_post():
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'sender':
        return'Not authorized', 401
    
    user = g.user  


    label_value = str(uuid.uuid4())

    label = create_label(label_value,user)
    try:
        db.hset(f"user:{user}:label", f"label_{label_value}", json.dumps(label))
    except Exception:
        return make_response("something went wrong.", 500)
    
    response = make_response("OK", 200)
    return response

@app.route('/sender/labels/<label_value>', methods=["DELETE"])
def labels_delete(label_value):
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'sender':
        return'Not authorized', 401
    
    user = g.user
    if not label_value:
        response = make_response("Bad Request, missing id ", 400)

    label = db.hget(f"user:{user}:label", f"label_{label_value}")
    if not label:
         return make_response("Not Exist", 404)
    state = json.loads(label.decode())['state']
    if state != 'nie nadana':
        return make_response("Wrong state", 400)
    if label_value:
        try:
            db.hdel(f"user:{user}:label", f"label_{label_value}")
        except Exception:
            return make_response("Oops!", 400)

    response = make_response("OK", 200)
    return response

###
@app.route('/courier/labels', methods=["GET"])
def labels():
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'courier':
        return'Not authorized', 401
    
    user = g.user  
    labels = get_all_labels(user)
    links = []
    items = []
    for key in labels:
        data = {'label':labels[key]['label'], 'status': labels[key]['state'], 'user': labels[key]['usr']}
        items.append(Embedded(data=data))
    document = Document(embedded={'items' : Embedded(data=items)},links=links)
    return document.to_json()

@app.route('/courier/packages', methods=["GET"])
def packages():
    if g.user is None:
        return'Not authorized', 401
    if g.user_role != 'courier':
        return'Not authorized', 401
    
    #user = g.user    
    packages = get_packages()
    
    items = []
    for key in packages:
        lab = packages[key]['label'] 
        data = {'label':lab, 'status': packages[key]['state'], 'user': packages[key]['user']}
        links = []
        links.append(Link('Post', f'courier/labels/{lab}', templated=False))
        links.append(Link('Update', f'courier/labels/{lab}', templated=False ))
        items.append(Embedded(data=data, links=links))
    document = Document(embedded={'items' : Embedded(data=items)})
    return document.to_json()

#http://127.0.0.1:5000/courier/packages/id?&user=login
@app.route('/courier/packages/<label_value>', methods=["POST"])
def package_Post(label_value):
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'courier':
        return'Not authorized', 401
     #user= g.user

    user = request.args.get('user') 

    label = db.hget(f"user:{user}:label", f"label_{label_value}")
    if label is None:
        return make_response("Label not Found",404)
        
    label = json.loads(label.decode())
    package = create_package(label)
    
    try:
        db.hset( f"packages", f"package_{label_value}", json.dumps(package))
    except Exception:
        return make_response("something went wrong.", 500)

    requests.post(f"{NOTIFI_SERVER_URL}/send", json={"userLogin" : user,"message": "A package was created"})
    return make_response("New Package created",200)

#http://127.0.0.1:5000/courier/packages/id?state=w drodze&user=login
@app.route('/courier/packages/<label_value>', methods=["PUT"])
def update_label(label_value):
    
    if g.user is None:
        return'Not authorized', 401
    
    if g.user_role != 'courier':
        return'Not authorized', 401
    new_state = request.args.get('state')
    user = request.args.get('user')
    if new_state != 'w drodze' and new_state != 'dostarczona' and new_state != 'odebrana':
         return make_response("Wrong state", 400)
    
    new_label = create_label(label_value,user,new_state)
    new_package = create_package(new_label)
    try:
        db.hset(f"user:{user}:label", f"label_{label_value}", json.dumps(new_label))
        db.hset( f"packages", f"package_{label_value}", json.dumps(new_package))
    except Exception:
        return make_response("something went wrong.", 500)   
    
    send_mess_to_queue(f'Package {label_value} has change status to {new_state} ','log')
    if new_state == 'odebrana':
         send_mess_to_queue(f'{label_value}','invoice')
    
    requests.post(f"{NOTIFI_SERVER_URL}/send", json={"userLogin" : user,"message": f"A package that have label " + label_value +  " change status to " + new_state})
    return make_response("OK",200)

def create_label(label, user, state = 'nie nadana'):
    label = {
        "label": label,
        "usr": user,
        "state": state
    }
    return label

def get_labels(user):
    labels = {}
    user_labels = db.hgetall(f"user:{user}:label")

    for id in user_labels:
        label = user_labels[id].decode()
        label = json.loads(label)
        labels[id.decode()] = label
    return labels

def get_all_labels(user):
    labels = {}
    users = db.hgetall('users')
    for login in users:
        user = users[login].decode()
        labels.update(get_labels(user))
    return labels

def create_package(label):
    package = {
        "label": label['label'],
        "user": label['usr'],
        "state": label['state']
    }
    return package

def get_packages():
    packages = {}
    user_packages = db.hgetall(f"packages")

    for id in user_packages:
        package = user_packages[id].decode()
        package = json.loads(package)
        packages[id.decode()] = package
    return packages


@app.before_request
def before_req_fun():
    token = request.headers.get('Authorization','').replace('Bearer ','')
    try:
        decoded = decode(token,JWT_SECRET, algorithms=['HS256'],audience='aud')
    except Exception as e:
        g.user = None
        send_mess_to_queue(f'Webserwer error. Unauthorized. {str(e)}', 'log')
        print('Unauthorized: ' + str(e))
        return None
    g.user = decoded['usr']

    g.user_role = decoded['user_role']



if __name__ == '__main__':
    app.run(threaded=True, port=5001, debug=True)




